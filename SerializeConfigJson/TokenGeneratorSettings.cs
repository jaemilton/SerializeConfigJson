﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SerializeConfigJson
{
    public class TokenGeneratorSettings
    {
        public TokenGeneratorSettings(
            string tokenUrl, 
            string clientId, 
            IList<string> athenticationHttpClientsNames,
            ClientCredentionTransmission clientCredentionTransmissionType,
            int refreshTimeBeforeTokenExpiration = 60,
            int tokenGeneratorTimeout = 500,
            string tokenServiceName = "identityserver"
        )
        {
            TokenUrl = tokenUrl;
            ClientId = clientId;
            AthenticationHttpClientsNames = athenticationHttpClientsNames;
            ClientCredentionTransmissionType = clientCredentionTransmissionType;
            RefreshTimeBeforeTokenExpiration = refreshTimeBeforeTokenExpiration;
            TokenGeneratorTimeout = tokenGeneratorTimeout;
            TokenServiceName = tokenServiceName;
        }

        public string TokenServiceName { get; private set; }
        public string TokenUrl { get; private set; }
        public string ClientId { get; private set; }
        public ClientCredentionTransmission ClientCredentionTransmissionType { get; private set; }
        public int RefreshTimeBeforeTokenExpiration { get; private set; }
        public int TokenGeneratorTimeout { get; private set; }
        public IList<string> AthenticationHttpClientsNames { get; private set; }

        public enum ClientCredentionTransmission
        {
            Body = 0,
            Header = 1
        }
    }

    
}
