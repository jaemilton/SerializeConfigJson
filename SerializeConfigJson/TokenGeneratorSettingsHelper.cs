﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static SerializeConfigJson.TokenGeneratorSettings;

namespace SerializeConfigJson
{
    public class TokenGeneratorSettingsConverter
    {

        public string TokenServiceName { get;  set; }
        public string TokenUrl { get;  set; }
        public string ClientId { get;  set; }
        public ClientCredentionTransmission ClientCredentionTransmissionType { get;  set; }
        public int RefreshTimeBeforeTokenExpiration { get;  set; }
        public int TokenGeneratorTimeout { get;  set; }
        public IList<string> AthenticationHttpClientsNames { get;  set; }

        
    }
    public static class TokenGeneratorSettingsHelper
    {

        public static List<TokenGeneratorSettings> TokenGeneratorSettingsList(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<TokenGeneratorSettingsConverter[]>(configuration.GetSection("ListTokenGeneratorSettings"));

            var listTokenGeneratorSettingsConverter =
                configuration.GetSection("ListTokenGeneratorSettings").Get<TokenGeneratorSettingsConverter[]>();

            return listTokenGeneratorSettingsConverter.ToList().ConvertAll(Convert);
        }

        private static TokenGeneratorSettings Convert(TokenGeneratorSettingsConverter source)
        {
            return new TokenGeneratorSettings
                    (
                        tokenUrl: source.TokenUrl,
                        clientId: source.ClientId,
                        athenticationHttpClientsNames: source.AthenticationHttpClientsNames,
                        clientCredentionTransmissionType: source.ClientCredentionTransmissionType,
                        refreshTimeBeforeTokenExpiration: source.RefreshTimeBeforeTokenExpiration,
                        tokenGeneratorTimeout: source.TokenGeneratorTimeout,
                        tokenServiceName: source.TokenServiceName
                    );
        }
    }

    
}
